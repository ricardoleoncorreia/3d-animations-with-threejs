# 3D Animations With ThreeJS

Quick guide to integrate ThreeJS with Angular.

## How to install the project locally?

1. Clone the [GitLab repo](https://gitlab.com/ricardoleoncorreia/3d-animations-with-threejs).
1. Change directory with `cd 3d-animations-with-threejs`.
1. Run `npm install`.
1. Run `npm start`.
1. Open `http://localhost:4200` in your browser.

## References

- [ThreeJS](https://threejs.org/).
