import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BoxGeometryDirective } from './directives/box-geometry.directive';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, BoxGeometryDirective],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'ThreeJS Demo For Angular'`, () => {
    const debugElement = fixture.debugElement;
    const titleElement: HTMLElement = debugElement.query(By.css('h1')).nativeElement;
    expect(titleElement.textContent).toBe('ThreeJS Demo for Angular');
  });

  it('should have a link to ThreeJS webpage', () => {
    const debugElement = fixture.debugElement;
    const anchorElement: HTMLElement = debugElement.query(By.css('a[href="https://threejs.org/"]')).nativeElement;
    expect(anchorElement.textContent).toBe('ThreeJS');
  });

  it('should have a link to the GitLab Repository', () => {
    const debugElement = fixture.debugElement;
    const repoAnchor = 'a[href="https://gitlab.com/ricardoleoncorreia/3d-animations-with-threejs/"]';
    const anchorElement: HTMLElement = debugElement.query(By.css(repoAnchor)).nativeElement;
    expect(anchorElement.textContent).toBe('GitLab Repo');
  });

  it('should have a container for the box geometry element', () => {
    const debugElement = fixture.debugElement;
    const boxContainerElement: HTMLElement = debugElement.query(By.directive(BoxGeometryDirective)).nativeElement;
    expect(boxContainerElement).toBeTruthy();
  });
});
