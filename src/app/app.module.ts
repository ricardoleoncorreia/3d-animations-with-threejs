import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BoxGeometryDirective } from './directives/box-geometry.directive';

@NgModule({
  declarations: [AppComponent, BoxGeometryDirective],
  imports: [BrowserModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
