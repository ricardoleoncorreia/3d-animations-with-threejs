import { Directive, ElementRef, OnInit } from '@angular/core';
import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  BoxGeometry,
  MeshBasicMaterial,
  Mesh,
  Color,
} from 'three';

@Directive({ selector: '[boxGeometry]' })
export class BoxGeometryDirective implements OnInit {
  constructor(private readonly element: ElementRef) {}

  ngOnInit(): void {
    const nativeElement: HTMLCanvasElement = this.element.nativeElement;
    const { clientHeight, clientWidth } = nativeElement;

    const scene = new Scene();
    scene.background = new Color(0xd3d3d3);

    const fieldOfView = 75;
    const aspect = clientWidth / clientHeight;
    const near = 0.1;
    const far = 1000;
    const camera = new PerspectiveCamera(fieldOfView, aspect, near, far);

    const renderer = new WebGLRenderer();
    renderer.setSize(clientWidth, clientHeight);
    nativeElement.appendChild(renderer.domElement);

    const geometry = new BoxGeometry();
    const material = new MeshBasicMaterial({ color: 0x274690 });
    const cube = new Mesh(geometry, material);
    scene.add(cube);

    camera.position.z = 5;

    function animate(): void {
      requestAnimationFrame(animate);

      cube.rotation.x += 0.01;
      cube.rotation.y += 0.01;

      renderer.render(scene, camera);
    }

    animate();
  }
}
